import React from 'react';
import _ from 'lodash';

import List from '../List';
import AddForm from '../AddForm';

let id = 0;

export default class extends React.Component {
  constructor({ list }) {
    super();

    id = _.reduce(list, (max, todo) => (
      Math.max(max, todo.id)
    ), 0);

    this.state = {
      list
    };
  }

  addTodo(text) {
    const { list } = this.state;

    list.push({
      id: ++id,
      completed: false,
      text
    });

    this.setState({ list });
  }

  render() {
    const { list } = this.state;

    return (
      <div>
        <List list = {list} />
        <AddForm onSubmit = {this.addTodo.bind(this)} />
      </div>
    );
  }
}
