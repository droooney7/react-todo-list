import React from 'react';

import './index.less';

export default ({
  completed,
  text,
  onToggle,
  onDelete
}) => (
  <li
    className = 'list-item list-group-item'
    style = {{
      textDecoration: completed ? 'line-through' : 'none'
    }}
  >
    <span
      className = 'text'
      onClick = {onToggle}
    >
      {text}
    </span>
    <span
      className = 'delete-icon glyphicon glyphicon-remove'
      onClick = {onDelete}
    >

    </span>
  </li>
);
