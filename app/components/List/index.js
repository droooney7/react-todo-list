import React from 'react';
import _ from 'lodash';

import Todo from '../Todo';

export default class extends React.Component {
  constructor({ list }) {
    super();

    this.state = {
      list
    };
  }

  toggleTodo(id) {
    const { list } = this.state;
    const item = _.find(list, (todo) => todo.id === id);

    if (item) {
      item.completed = !item.completed;

      this.setState({ list });
    }
  }

  deleteTodo(id) {
    const { list } = this.state;
    const index = _.findIndex(list, (todo) => todo.id === id);

    if (index !== -1) {
      list.splice(index, 1);

      this.setState({ list });
    }
  }

  render() {
    const { list } = this.state;

    return (
      <ul className = 'list-group col-sm-2'>
        {list.map(({ id, completed, text }, index) => (
          <Todo
            key = {id}
            completed = {completed}
            text = {text}
            onToggle = {this.toggleTodo.bind(this, id)}
            onDelete = {this.deleteTodo.bind(this, id)}
          />
        ))}
      </ul>
    );
  }
}
