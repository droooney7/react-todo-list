import React from 'react';

export default function ({ onSubmit }) {
  let input;

  return (
    <form
      className = 'form-inline'
      onSubmit = {(e) => {
        e.preventDefault();

        onSubmit(input.value);

        input.value = '';
      }}
    >
      <div className = 'form-group'>
        <label
          className = 'form-group'
          htmlFor = 'add-todo'
        >
          To do:
        </label>
        <input
          id = 'add-todo'
          className = 'form-control'
          ref = {(node) => input = node}
          required
        />
      </div>
      <div className = 'form-group'>
        <input
          className = 'form-control btn btn-success'
          type = 'submit'
          value = 'Add'
        />
      </div>
    </form>
  );
}
