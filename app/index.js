import React from 'react';
import { render } from 'react-dom';

import App from './components/App';
import TodoList from './components/TodoList';

import 'bootstrap/less/bootstrap.less';

const list = [];

render(
  <App>
    <TodoList list = {list} />
  </App>,
  document.getElementById('root')
);
